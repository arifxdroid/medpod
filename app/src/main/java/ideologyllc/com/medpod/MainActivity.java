package ideologyllc.com.medpod;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import ideologyllc.com.medpod.backendProcess.NewMedPodProvisioning;
import ideologyllc.com.medpod.connectivity.NetworkUtil;
import ideologyllc.com.medpod.db.DBHelper;
import ideologyllc.com.medpod.helperClass.ServiceHelper;
import ideologyllc.com.medpod.location.LocationTraker;
import ideologyllc.com.medpod.modelClass.DefaultSetting;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private NetworkUtil networkUtil;
    private String serverStatus = "notOk";
    private String url;

    private NewMedPodProvisioning newMedPodProvisioning;
    private DBHelper dbHelper;
    private DefaultSetting defaultSetting;
    private static MainActivity ins;
    Context context = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ins = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        newMedPodProvisioning = new NewMedPodProvisioning(this);
        dbHelper = new DBHelper(this);
        networkUtil = new NetworkUtil(getApplicationContext());
        defaultSetting = new DefaultSetting();


        if (networkUtil.isConnectingToInternet(getApplicationContext())) {
            String netState = networkUtil.getConnectivityStatusString(getApplicationContext());
            Toast.makeText(this, "Connected and " + netState, Toast.LENGTH_SHORT).show();
            connectingIPAddress();
        } else {
            Toast.makeText(this, "Not Connected", Toast.LENGTH_SHORT).show();
            networkOptionDialogue();
        }

    }

    public static MainActivity getIns() {
        return ins;
    }


    // Open Dialog for network options
    public void networkOptionDialogue() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.network_connection);
        dialog.setTitle("Network Connection");

        // setting up custom dialog components - text, image and button
        Switch switchWifi = (Switch) dialog.findViewById(R.id.switchWifi);
        Switch switchMobile = (Switch) dialog.findViewById(R.id.switchMobile);

        switchWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    progressDialog = ProgressDialog.show(MainActivity.this, "WiFi", "Connecting...");
                    networkUtil.toggleWiFi(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Thread.sleep(10000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (networkUtil.isConnectingToInternet(getApplicationContext())) {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "connected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            } else {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "notConnected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            }
                        }
                    }).start();

                } else {
                    Toast.makeText(MainActivity.this, "Off", Toast.LENGTH_LONG).show();
                }
            }
        });


        switchMobile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    progressDialog = ProgressDialog.show(MainActivity.this, "Mobile Data", "Connecting...");
                    try {
                        networkUtil.toggleMobileData(true);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    } catch (NoSuchFieldException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                Thread.sleep(12000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (networkUtil.isConnectingToInternet(getApplicationContext())) {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "connected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            } else {
                                Message msg = Message.obtain();
                                Bundle b = new Bundle();
                                b.putString("state", "notConnected");
                                msg.setData(b);
                                handler.sendMessage(msg);
                                dialog.dismiss();
                            }
                        }
                    }).start();

                } else {
                    Toast.makeText(MainActivity.this, "Off", Toast.LENGTH_LONG).show();
                }
            }
        });

        // Dialog closing button
        Button buttonClose = (Button) dialog.findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //Restart Activity
    public void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    //Handler
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            String netState;
            netState = msg.getData().getString("state");
            switch (netState) {

                case "connected":
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "connected", Toast.LENGTH_LONG).show();
                    restartActivity();
                    break;
                case "notConnected":
                    progressDialog.dismiss();
                    Toast.makeText(MainActivity.this, "Connection problem", Toast.LENGTH_LONG).show();
                    restartActivity();
                    break;
            }
        }
    };


    private void connectingIPAddress() {
        String conn = networkUtil.getConnectivityStatusString(MainActivity.this);
        switch (conn) {
            case "Wifi enabled":
                url = "http://public.asaduzzamankochi.me/";
                break;
            case "Mobile data enabled":
                url = "http://private.asaduzzamankochi.me/";
                break;
            default:
                break;
        }

        connectURL(url);
    }

    private void connectURL(String url) {
        progressDialog = ProgressDialog.show(MainActivity.this, "Please wait...", "Connecting to the server...", true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj1 = new JSONObject(response);
                            JSONObject obj2 = obj1.getJSONObject("1");
                            serverStatus = obj2.getString("status");

                            Toast.makeText(getApplicationContext(), serverStatus,
                                    Toast.LENGTH_LONG).show();

                            if (dbHelper.isProvisioned()) {
                                Intent intent = new Intent(MainActivity.this, DefaultActivity.class);
                                startActivity(intent);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                finish();
                            } else {

                                newMedPodProvisioning.initializeMedPod(new Intent(MainActivity.this, DefaultActivity.class));

//                                    Intent intent = new Intent(MainActivity.this, DefaultActivity.class);
//                                    startActivity(intent);
//                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                    finish();


                            }


                        } catch (JSONException e) {
                            Toast.makeText(getApplicationContext(), "Failed",
                                    Toast.LENGTH_LONG).show();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        customDialogue("Connection Failed", "Server Error", "OK");
                    }
                });
        ServiceHelper.getInstance().addToRequestQueue(stringRequest);

    }

    public void customDialogue(String title, String message, String button) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(button, null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
