package ideologyllc.com.medpod.error;

import android.content.Context;

/**
 * Created by ${Arif} on 11/23/2015.
 */
public class ErrorHandler {

    ErrorHandler errorHandler;
    Context context;

    private ErrorHandler(){
        errorHandler = this;
    }

    public ErrorHandler getInstance(Context context){
        this.context = context;
        return errorHandler;
    }

    public void networkError(){

    }

    public void locationError(){

    }

    public void cloudError(){

    }

    public void sslConnectionError(){

    }
}
