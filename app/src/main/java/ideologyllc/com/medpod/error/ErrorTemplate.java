package ideologyllc.com.medpod.error;

/**
 * Created by ${Arif} on 11/23/2015.
 */
public class ErrorTemplate {

    int id;
    String date;
    String time;
    String error;
    String medpodId;
    String errorMessage;

    public ErrorTemplate(int id, String date, String time, String error, String medpodId, String errorMessage) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.error = error;
        this.medpodId = medpodId;
        this.errorMessage = errorMessage;
    }

    public ErrorTemplate(String date, String time, String error, String medpodId, String errorMessage) {
        this.date = date;
        this.time = time;
        this.error = error;
        this.medpodId = medpodId;
        this.errorMessage = errorMessage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMedpodId() {
        return medpodId;
    }

    public void setMedpodId(String medpodId) {
        this.medpodId = medpodId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "["+date+"  "+time+"] "+"["+error+"] "+"["+medpodId+"] "+"["+errorMessage+"] ";
    }
}
