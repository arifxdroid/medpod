package ideologyllc.com.medpod;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ideologyllc.com.medpod.call.CallHandler;
import ideologyllc.com.medpod.db.DBHelper;
import ideologyllc.com.medpod.modelClass.DefaultSetting;
import ideologyllc.com.medpod.sms.SMSHandler;

public class DefaultActivity extends AppCompatActivity {

    private String voiceCallNumber;
    private String sendSmsNumber;
    private String messageText;
    private SMSHandler smsHandler;
    private CallHandler callHandler;
    private DBHelper dbHelper;
    private DefaultSetting defaultSetting;
    private TextView txtMedId;
    private TextView txtHelpLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);

        smsHandler = new SMSHandler(getApplicationContext());
        callHandler = new CallHandler(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        dbHelper = new DBHelper(this);
        defaultSetting = new DefaultSetting();

        if (!dbHelper.getDefaultSetting().isEmpty()) {
            customDialogue(dbHelper.getDefaultSetting().get(0).getHelpLine(), dbHelper.getDefaultSetting().get(0).getMedpodId(), "OK");
        }

    }

    public void customDialogue(String title, String message, String button) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(button, null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void sendSms(){

        final Dialog dialogSMS = new Dialog(this);
        dialogSMS.setContentView(R.layout.sms_sending);
        dialogSMS.setTitle("Send SMS");

        final EditText etMessageBody = (EditText)dialogSMS.findViewById(R.id.etMessageBody);
        Button btnSendSms = (Button)dialogSMS.findViewById(R.id.btnSendSms);

        btnSendSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageBody = etMessageBody.getText().toString();
                String number = dbHelper.getDefaultSetting().get(0).getHelpLine();
                if (messageBody.equals("")){
                    Toast.makeText(getApplicationContext(), "Text field is empty, Please input your text", Toast.LENGTH_LONG).show();
                }else {
                    smsHandler.sendSMS(number, messageBody);
                    dialogSMS.dismiss();
                }
            }
        });

        dialogSMS.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_default, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_voiceCall:
                callHandler.voiceCall(dbHelper.getDefaultSetting().get(0).getHelpLine());
                break;
            case R.id.action_sendSms:
                sendSms();
                break;
            case R.id.action_videoCall:
                break;
        }
        return true;
    }
}
