package ideologyllc.com.medpod.modelClass;

/**
 * Created by Kochi on 25-Nov-15.
 */
public class DefaultSetting {

    private int id;
    private String medpodId;
    private String helpLine;

    public DefaultSetting() {
    }

    public DefaultSetting(String helpLine) {
        this.helpLine = helpLine;
    }

    public DefaultSetting(String medpodId, String helpLine) {
        this.medpodId = medpodId;
        this.helpLine = helpLine;
    }

    public DefaultSetting(int id, String medpodId, String helpLine) {
        this.id = id;
        this.medpodId = medpodId;
        this.helpLine = helpLine;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMedpodId() {
        return medpodId;
    }

    public void setMedpodId(String medpodId) {
        this.medpodId = medpodId;
    }

    public String getHelpLine() {
        return helpLine;
    }

    public void setHelpLine(String helpLine) {
        this.helpLine = helpLine;
    }
}
