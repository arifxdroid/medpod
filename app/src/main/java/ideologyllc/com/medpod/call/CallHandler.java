package ideologyllc.com.medpod.call;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneStateListener;
import android.widget.Toast;

/**
 * Created by ${Arif} on 11/23/2015.
 */
public class CallHandler {

    private Context context;

    public CallHandler(Context context) {
        this.context = context;
    }

/**
 * Method for voice calling, Require parameter number as string
**/
    public void voiceCall(String number){

        try {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + number));
            context.startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));


        } catch (SecurityException e) {
            Toast.makeText(context, "Your call has failed...",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

}
