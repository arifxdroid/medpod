package ideologyllc.com.medpod.backendProcess;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import ideologyllc.com.medpod.DefaultActivity;
import ideologyllc.com.medpod.MainActivity;
import ideologyllc.com.medpod.connectivity.NetworkUtil;
import ideologyllc.com.medpod.db.DBHelper;
import ideologyllc.com.medpod.helperClass.ServiceHelper;
import ideologyllc.com.medpod.location.LocationTraker;
import ideologyllc.com.medpod.modelClass.DefaultSetting;

/**
 * Created by Kochi on 25-Nov-15.
 */
public class NewMedPodProvisioning {
    Context context;
    private String deviceId;
    private String medPodId;
    private String helpline;
    private static double latitude;
    private static double longitude;
    private ProgressDialog progressDialog;
    private String url;
    private DBHelper dbHelper;
    private NetworkUtil networkUtil;
    private String status;
    private DefaultSetting defaultSetting;


    public NewMedPodProvisioning(Context context) {
        this.context = context;
        dbHelper = new DBHelper(context);
        networkUtil = new NetworkUtil(context);
        defaultSetting = new DefaultSetting();
    }


    //Getting unique device id
    private String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    //Getting Current Location
    private void getMyLocation() {
        LocationTraker traker = new LocationTraker(context);

        if (traker.getState()) {
            latitude = traker.getlat();
            longitude = traker.getLon();

        }
    }

    private void connectURL(String url, final Intent intent) {
        progressDialog = ProgressDialog.show(context, "Please wait...", "First time provisioning...", true);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject obj1 = new JSONObject(response);
                            JSONObject obj2 = obj1.getJSONObject("1");
                            defaultSetting.setMedpodId(obj2.getString("medpodID"));
                            defaultSetting.setHelpLine(obj2.getString("default_number"));
                            status = obj2.getString("status");
//                            customDialogue(defaultSetting.getMedpodId(), "HelpLine:" + defaultSetting.getHelpLine() + ",Lat:" + latitude + ", Lon:" + longitude, status);
                            if (status.equals("0")) {
                                storeIntoLocalDatabase();
                            } else if (status.equals("1") || status.equals("2")) {
                                customDialogue("Thing Creation Failed", "Server Error", "OK");
                            } else {
                                customDialogue("Thing Creation Failed", "Server Error", "OK");
                            }

                        } catch (JSONException e) {
                            customDialogue("Server Error", "", "OK");

                        }

                        context.startActivity(intent);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        MainActivity.getIns().finish();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        customDialogue("Connection Failed", "Server Error", "OK");

                    }
                });
        ServiceHelper.getInstance().addToRequestQueue(stringRequest);

    }

    private void storeIntoLocalDatabase() {
        dbHelper.insertIntoDefaultSetting(defaultSetting);
    }

    private void customDialogue(String title, String message, String button) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton(button, null);
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void initializeMedPod(Intent intent) {
        String returnStatus;
        deviceId = this.getDeviceId();
        this.getMyLocation();
//        customDialogue(deviceId,"Lat:"+latitude+", Lon:"+longitude,"OK");
        String conn = networkUtil.getConnectivityStatusString(context);
        switch (conn) {
            case "Wifi enabled":
                url = "http://public.asaduzzamankochi.me/newMedPod.php?medpodId=" + deviceId + "&latitude=" + latitude + "&longitude=" + longitude;
                break;
            case "Mobile data enabled":
                url = "http://private.asaduzzamankochi.me/newMedPod.php?medpodId=" + deviceId + "&latitude=" + latitude + "&longitude=" + longitude;
                break;
            default:
                break;
        }
        connectURL(url,intent);
//        storeIntoLocalDatabase();
    }


}
