package ideologyllc.com.medpod.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import ideologyllc.com.medpod.modelClass.DefaultSetting;


public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "medpodapp.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = null;


    public static final String TABLE_NAME = "default_setting";
    public static final String ID_FIELD = "_ID";
    public static final String MEDPOD_ID = "medpodId";
    public static final String MEDPOD_HELPLINE = "helpLine";
    public static final String TABLE_SQL = "CREATE TABLE " + TABLE_NAME + " (" + ID_FIELD + " INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, " + MEDPOD_ID + " TEXT, " + MEDPOD_HELPLINE + " TEXT)";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_SQL);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public long insertIntoDefaultSetting(DefaultSetting defaultSetting) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MEDPOD_ID, defaultSetting.getMedpodId());
        values.put(MEDPOD_HELPLINE, defaultSetting.getHelpLine());

        long inserted = sqLiteDatabase.insert(TABLE_NAME, null, values);
        sqLiteDatabase.close();
        return inserted;
    }

    public boolean isProvisioned() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
        ArrayList<DefaultSetting> all = new ArrayList<DefaultSetting>();

        if (cursor != null) {

            if (cursor.getCount() > 0) {

                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
                    String medpodId = cursor.getString(cursor.getColumnIndex(MEDPOD_ID));
                    String helpline = cursor.getString(cursor.getColumnIndex(MEDPOD_HELPLINE));

                    DefaultSetting setting = new DefaultSetting(id, medpodId, helpline);
                    all.add(setting);

                } while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();

        if (all.size() > 0) {
            return true;
        }
        return false;
    }

    public String getHelplineNumber() {
        String helpLine = "";
        ArrayList<DefaultSetting> medPodInfo = new ArrayList<DefaultSetting>();
        String query = "SELECT " + MEDPOD_HELPLINE + " FROM " + TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                // get  the  data into array,or class variable
                medPodInfo.add(new DefaultSetting(cursor.getString(cursor.getColumnIndex(MEDPOD_HELPLINE))));
            } while (cursor.moveToNext());
        }
        sqLiteDatabase.close();

        helpLine = medPodInfo.get(0).getHelpLine();

        return helpLine;
    }

    public String getMedpodIdNumeber() {
        String medPodId = "";
        ArrayList<DefaultSetting> medPodInfo = new ArrayList<DefaultSetting>();
        String query = "SELECT " + MEDPOD_ID + " FROM " + TABLE_NAME;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                // get  the  data into array,or class variable
                medPodInfo.add(new DefaultSetting(cursor.getString(cursor.getColumnIndex(MEDPOD_ID))));
            } while (cursor.moveToNext());
        }
        sqLiteDatabase.close();

        medPodId = medPodInfo.get(0).getMedpodId();

        return medPodId;
    }

    public ArrayList<DefaultSetting> getDefaultSetting() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);
        ArrayList<DefaultSetting> all = new ArrayList<DefaultSetting>();

        if (cursor != null) {

            if (cursor.getCount() > 0) {

                cursor.moveToFirst();
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(ID_FIELD));
                    String medpodId = cursor.getString(cursor.getColumnIndex(MEDPOD_ID));
                    String helpline = cursor.getString(cursor.getColumnIndex(MEDPOD_HELPLINE));

                    DefaultSetting setting = new DefaultSetting(id, medpodId, helpline);
                    all.add(setting);

                } while (cursor.moveToNext());
            }
        }

        db.close();
        cursor.close();
        return all;
    }


//    public ArrayList<Employee> showEmployeeInformation(int id) {
//        ArrayList<Employee> employeeInformation = new ArrayList<Employee>();
//        String query = "SELECT * FROM employee_info WHERE id ='" + id + "'";
//        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
//        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
//        if (cursor.moveToFirst()) {
//            do {
//                // get  the  data into array,or class variable
//                employeeInformation.add(new Employee(cursor.getInt(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("empId")), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("designation")), cursor.getString(cursor.getColumnIndex("contact")), cursor.getString(cursor.getColumnIndex("email"))));
//            } while (cursor.moveToNext());
//        }
//        sqLiteDatabase.close();
//        return employeeInformation;
//    }
//
//    public void updateEmployeeInformation(int id, String empId, String name, String designation, String contact, String email) {
//
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
//        ContentValues values = new ContentValues();
//        values.put("empId", empId);
//        values.put("name", name);
//        values.put("designation", designation);
//        values.put("contact", contact);
//        values.put("email", email);
//
//        sqLiteDatabase.update("employee_info", values, "id= ?", new String[]{Integer.toString(id)});
//
//    }
//
//    public void deleteEmployeeInformation(int id) {
//        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
//        sqLiteDatabase.delete("employee_info", "id= ?", new String[]{Integer.toString(id)});
//    }
}



